-- SUMMARY --

The random_nids module allows you to replace your site's node nids with a random value of 7 digits in order
to make it impossible to keep track of the node amount.

For a full description of the module, visit the project page:
  http://drupal.org/project/random_nids

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/random_nids


-- REQUIREMENTS --

The Data contrib module


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Customize the module's settings in Admin menu » Site Configuration » Random nids
  Administration » Administration menu.

-- CONTACT --

Current maintainers:
* Yuval Perelman (perelman.yuval) - http://drupal.org/user/622934
* Royi Benyossef (royiby) - http://drupal.org/user/717016

This project has been sponsored by:
* Linnovate
  Drupal experts Visit http://linnovate.net for more information.

